#![feature(proc_macro_hygiene, decl_macro)]

#[macro_use] extern crate rocket;

use rocket_contrib::templates::Template;
use rocket_contrib::serve::StaticFiles;
use std::collections::HashMap;

mod json_types;
use crate::json_types::{SyrBackendProjectsResponse, SyrBackendExperiencesResponse, SyrBackendServicesResponse};

#[get("/")]
fn index() -> Template {
    let mut context: HashMap<&str, &str> = HashMap::new();
    context.insert("page_title", "Home");
    Template::render("home", &context)
}

#[get("/contact")]
fn contact() -> Template {
    let mut context: HashMap<&str, &str> = HashMap::new();
    context.insert("page_title", "Contact");
    Template::render("contact", &context)
}

#[get("/services")]
fn services() -> Template {
    let mut context: HashMap<&str, &str> = HashMap::new();
    context.insert("page_title", "Services");
    Template::render("services", &context)
}

#[get("/experience")]
fn experience() -> Template {
    let mut context: HashMap<&str, &str> = HashMap::new();
    context.insert("page_title", "Professional Experience");
    Template::render("experience", &context)
}

#[get("/projects")]
fn projects() -> Template {
    let mut context: HashMap<&str, &str> = HashMap::new();
    context.insert("page_title", "Portfolio");
    Template::render("projects", &context)
}

#[catch(404)]
fn not_found() -> Template {
    let mut context: HashMap<&str, &str> = HashMap::new();
    context.insert("page_title", "Not Found");
    Template::render("404", &context)
}

fn main() {
    rocket::ignite()
        .mount("/", routes![index, contact, services, experience, projects])
        .mount("/assets", StaticFiles::from("./assets"))
        .register(catchers![not_found])
        .attach(Template::fairing())
        .launch();
}
