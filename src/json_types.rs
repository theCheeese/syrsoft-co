use serde::Deserialize;
use serde_json::value::Number;

#[derive(Deserialize, Debug)]
pub struct SyrBackendExperiencesResponse {
    pub experiences: Vec<Experience>
}

#[derive(Deserialize, Debug)]
pub struct Experience {
    id: Number,
    company: String,
    title: String,
    date_start: String,
    date_end: Option<String>,
    logo: Option<String>
}

#[derive(Deserialize, Debug)]
pub struct SyrBackendProjectsResponse {
    pub projects: Vec<Project>
}

#[derive(Deserialize, Debug)]
pub struct Project {
    id: Number,
    name: String,
    role: Option<String>,
    date_start: String,
    date_end: Option<String>,
    link: Option<String>,
    logo: Option<String>
}

#[derive(Deserialize, Debug)]
pub struct SyrBackendServicesResponse {
    services: Vec<Service>
}

#[derive(Deserialize, Debug)]
pub struct Service {
    id: Number,
    name: String,
    headline: Option<String>,
    note: Option<String>,
    date_start: String,
    date_end: Option<String>,
    link: Option<String>,
    logo: Option<String>
}
